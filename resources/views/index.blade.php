<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Post your posts</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

</head>

<body class="bg-purple-300 text-black-700 ">
    <div>
        <h1 class="text-center mt-10 text-5xl font-extrabold">Post Your Posts</h1>
    </div>
    <div class="container mx-auto px-4">
        <div class="grid grid-cols-3 my-10">
            @foreach($posts as $post)
            <div class="bg-white hover:bg-green-300 border border-gray-200 rounded-lg p-5 mb-4">
                <p class="text-xs text-right text-blue-900 underline">{{ $post->published_at }}</p>
                <h3 class="font-bold text-md mb-4"><b class="text-purple-600">Title:</b> {{ $post->title }}</h3>
                <span class="text-xs">{{ $post->excerpt }}</span>
            </div>
            @endforeach
        </div>

        <div class="mb-10">
            {{ $posts->links() }}
        </div>
    </div>

</body>

</html>
