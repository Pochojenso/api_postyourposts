<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Post your posts</title>
    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

</head>

<body class="bg-purple-300 text-black-700">
    <div>
        <h1 class="text-center mt-10 text-5xl font-extrabold">Users</h1>
    </div>
    <div class="container mx-auto px-4">
        <div class="grid grid-cols-2 my-10">
            @foreach($users as $user)
            <ul class="bg-white hover:bg-yellow-100 border border-pink-500 rounded-lg p-5 mb-4">
                <li class="text-center text-purple-900"><b>{{ $user->name }}</b></li>
                <li class="text-xs text-right text-blue-900">{{ $user->email }}</li>
            </ul>
            @endforeach
        </div>

        <div class="mb-10">
            {{ $users->links() }}
        </div>
    </div>

</body>

</html>
